dynare_config;

generate_data;

h1 = figure(1);
plot(ts0), hold on, plot(ts3), hold off
saveas(h1,'plt01','pdf')

h2 = figure(2);
plot(ts1,ts2,'ok')
hold on, plot([-4 4],[-4 4],'-r'), hold off
axis tight
saveas(h2,'plt02','pdf')