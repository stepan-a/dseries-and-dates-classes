ROOT_PATH = .

plot-build:
	$(MAKE) -C $(ROOT_PATH)/matlab all

slides-build:
	$(MAKE) -C $(ROOT_PATH)/tex all

all: plot-build slides-build

cleanall:
	$(MAKE) -C $(ROOT_PATH)/tex cleanall
	$(MAKE) -C $(ROOT_PATH)/matlab cleanall

tarball:
	tar -pczf ../dates-and-dseries-classes.tar.gz . --exclude .git
