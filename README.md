Slides introducing the dates and dseries class in Dynare.

To build slides.pdf you need to create a file called `./local.mk`
where the paths to matlab and dynare are defined. A typical `local.mk`
file will look like:

```make
MATLAB = /usr/bin/matlab
DYNARE = /home/stepan/works/dynare/matlab
```

Once this is done you just need to type the following instruction:

```bash
$ make all
```
